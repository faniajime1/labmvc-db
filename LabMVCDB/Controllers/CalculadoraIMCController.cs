﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Laboratorio3.Models;

namespace Laboratorio3.Controllers
{
    public class CalculadoraIMCController : Controller
    {
        private PersonaModel[] personas;
        public ActionResult ResultadoIMC()
        {
            PersonaModel persona = new PersonaModel(1, "Cristiano Ronaldo", 84.0, 1.87);
            double IMC = persona.Peso / (persona.Estatura * persona.Estatura);
            ViewBag.IMC = IMC;
            ViewBag.persona = persona;
            return View();
        }
        public ActionResult ResultadosAleatoriosIMC()
        {
            Random random = new Random();
            personas = new PersonaModel[20];
            double[] IMCs = new double[20];
            for (int p = 0; p < 20; p++)
            {
                double peso = random.NextDouble() * (150 - 20) + 20;
                double estatura = random.NextDouble() * (2 - 1) + 1;
                PersonaModel persona = new PersonaModel(p, "Sin Nombre", peso, estatura);
                personas[p] = persona;
                IMCs[p] = personas[p].Peso / (personas[p].Estatura * personas[p].Estatura);
            }
            ViewBag.personas = personas;
            ViewBag.IMCs = IMCs;
            return View();
        }
    }
}
