﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laboratorio3.Models
{
    public class PersonaModel
    {
        public string Nombre { get; set; }
        public int id { get; set; }
        public double Peso { get; set; }
        public double Estatura { get; set; }

        public PersonaModel()
        {
            this.Nombre = "Sin nombre";
            this.id = 0;
            this.Peso = 0.0;
            this.Estatura = 0.0;
        }

        public PersonaModel(int id, string Nombre, double Peso, double Estatura)
        {
            this.Nombre = Nombre;
            this.id = id;
            this.Peso = Peso;
            this.Estatura = Estatura;
        }

    }
}
